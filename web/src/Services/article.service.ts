import {Injectable} from '@angular/core';
import {GLOBAL1} from "../app/app_config";
import {Article} from "../Models/Article";
import {Member} from "../Models/Member";
import {Outil} from "../Models/Outil";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {MemberService} from "./member.service";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private httpClient: HttpClient,private MS:MemberService,private router: Router) {
  }

  saveArticle(article: Article): void {
    const articleToSave = {
      ...article,
    };
    if(articleToSave?.idArticle != null)
    {
      this.httpClient.put<Member>('http://localhost:9000/labo/api/updateArticle/'+articleToSave.idArticle ,articleToSave).toPromise().then(() => this.router.navigate(['./article'])) ;
    }
    else
    {
      articleToSave.idMembre=localStorage.getItem("loggedId");
      this.MS.getMemberlazybyId(localStorage.getItem("loggedId")).then((item: Member) => {
        articleToSave.membreDTO=item;
        this.httpClient.post<Article>('http://localhost:9000/labo/api/addArticle',articleToSave).toPromise().then(() => this.router.navigate(['./article'])) ;

      });
    }
  }
  remove(id: string):Promise<any>{
    return this.httpClient.delete<void>('http://localhost:9000/labo/api/deleteArticle/' + id).toPromise();
  }
  getArticlebyId(id: string|null): Promise<any> {
    return  this.httpClient.get<Article>('http://localhost:9000/labo/api/article/'+id).toPromise();
  }
  getAllArticles():Promise<Article[] | undefined>{
    return this.httpClient.get<Article[]>('http://localhost:9000/labo/api/articles').toPromise();
  }
}
