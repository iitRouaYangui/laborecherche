import { Injectable } from '@angular/core';
import {Outil} from "../Models/Outil";
import {GLOBAL1} from "../app/app_config";
import {Evenement} from "../Models/Evenement";
import {Article} from "../Models/Article";
import {HttpClient} from "@angular/common/http";
import {MemberService} from "./member.service";
import {Member} from "../Models/Member";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class EventService {
  constructor(private httpClient : HttpClient, private memberService : MemberService,private router:Router) {
  }

  //public tab : Evenement[]=GLOBAL1.DB1.event;
  saveEvent(event:Evenement):void{

    const eventToSave ={
      ...event,
    };
    if(eventToSave?.id != null)
    {
       this.httpClient.put<Evenement>('http://localhost:9000/labo/api/updateEvent/'+eventToSave.id ,eventToSave).toPromise().then(() => this.router.navigate(['./event']));
    }
    else
    {
      console.log(eventToSave);
      eventToSave.idMembre= localStorage.getItem("loggedId");
      this.memberService.getMemberlazybyId(localStorage.getItem("loggedId")).then((item:Member) =>{
        eventToSave.membreDTO=item;
         this.httpClient.post<Evenement>('http://localhost:9000/labo/api/addEvent',eventToSave).toPromise().then(() => this.router.navigate(['./event']));
      });
    }
  }
  remove(id: string): Promise<void> {
    return this.httpClient.delete<void>('http://localhost:9000/labo/api/deleteEvent/' + id).toPromise();
  }
  getEventyId(id: string): Promise<any> {

    return  this.httpClient.get<Evenement>('http://localhost:9000/labo/api/event/'+id).toPromise();
  }
  getEvents():Promise<Evenement[] | undefined>{

    return this.httpClient.get<Evenement[]>('http://localhost:9000/labo/api/events').toPromise();
  }

}
