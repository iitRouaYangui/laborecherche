import { Injectable } from '@angular/core';
import {GLOBAL1} from "../app/app_config";
import {Outil} from "../Models/Outil";
import {Evenement} from "../Models/Evenement";
import {Member} from "../Models/Member";
import {HttpClient} from "@angular/common/http";
import {MemberService} from "./member.service";
import {Router} from "@angular/router";



@Injectable({
  providedIn: 'root'
})
export class ToolsService {
 // public tab : Outil[]=GLOBAL1.DB1.outils;


  // saveTools(outil:Outil):Promise<any>{
  //   let member ;
  //   const outilToSave ={
  //     ...outil,
  //   };
  //  // outilToSave.id = outilToSave.id?? Math.ceil((Math.random() * 10000)).toString();
  //   outilToSave.idMembre=localStorage.getItem("loggedId")
  //   // this.MS.getMemberlazybyId(localStorage.getItem("loggedId")).then((item: Member) => {
  //   //   member=item
  //   //   outilToSave.membreDTO=member;
  //   // });
  //
  //   return this.httpClient.post<Outil>('http://localhost:9000/labo/api/addTool',outilToSave).toPromise();
  //
  // }

  saveTools(outil:Outil):void{
    const outilToSave ={
         ...outil,
        };

    if(outilToSave?.id != null)
    {
      this.httpClient.put<Member>('http://localhost:9000/labo/api/updateTool/'+outilToSave.id ,outilToSave).toPromise().then(() => this.router.navigate(['./Tools'])) ;
    }
    else
    {
    outilToSave.idMembre=localStorage.getItem("loggedId");
    this.MS.getMemberlazybyId(localStorage.getItem("loggedId")).then((item: Member) => {
    outilToSave.membreDTO=item;
      this.httpClient.post<Outil>('http://localhost:9000/labo/api/addTool',outilToSave).toPromise().then(() => this.router.navigate(['./Tools'])) ;

     });
    }

  }
  getOutilbyId(id: string|null): Promise<any> {
    return  this.httpClient.get<Outil>('http://localhost:9000/labo/api/tool/'+id).toPromise();
  }
  remove(id: string):Promise<any>{
    return this.httpClient.delete<void>('http://localhost:9000/labo/api/deleteTool/' + id).toPromise();
  }

  getAllOutils():Promise<Outil[] | undefined>{
    return this.httpClient.get<Outil[]>('http://localhost:9000/labo/api/tools').toPromise();
  }

  constructor(private httpClient: HttpClient ,private MS:MemberService ,private router: Router  ) {



  }
}
