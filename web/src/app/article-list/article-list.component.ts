import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {DialogBoxComponent} from "../dialog-box/dialog-box.component";
import {ArticleService} from "../../Services/article.service";
import {Article} from "../../Models/Article";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../../Services/auth.service";

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  displayedColumns: string[] = ['type', 'title', 'lien', 'date' ,'sourcepdf','member' , 'action'];
  dataSource: MatTableDataSource<Article>;
  liste:any
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private AS: ArticleService, private router: Router, public dialog: MatDialog ,private toastr: ToastrService, public authService : AuthService) {
    this.dataSource = new MatTableDataSource(this.liste);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  delete(id: string) {
    const dialogRef = this.dialog.open(DialogBoxComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

        this.AS.remove(id).then(() => this.getArticles());

      }
    });
  }
  downloadPDF(pdf:string , name:string) {
    this.toastr.success('PDF is downloading !', 'Download');
    const linkSource = pdf;
    const downloadLink = document.createElement("a");
    const fileName = name+".pdf";

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();

  }

  getArticles()
  {
    this.AS.getAllArticles().then((data)=>
    {
      this.liste=data;
      this.dataSource.data=this.liste;
    });
  }

  ngOnInit(): void {
    this.getArticles();

  }
}

