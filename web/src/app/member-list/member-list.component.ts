import {Component, OnInit, ViewChild} from '@angular/core';
import {Member} from "../../Models/Member";
import {MemberService} from "../../Services/member.service";
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from "@angular/material/table";
import {DialogBoxComponent} from "../dialog-box/dialog-box.component";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../../Services/auth.service";
import {HttpClient} from "@angular/common/http";
@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css']
})
export class MemberListComponent implements OnInit {

  public date = new Date();
  liste: any;
  userLoggedIn = false;
  displayedColumns: string[] = ['cin', 'name', 'type','cv','createdDate','action'];
  dataSource:MatTableDataSource<Member>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private MS : MemberService ,private router: Router, public dialog: MatDialog
              ,private toastr: ToastrService, public authService : AuthService, private http: HttpClient) {
    this.dataSource = new MatTableDataSource(this.liste);
  }
  delete(id: string) {
    const dialogRef = this.dialog.open(DialogBoxComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

        this.MS.remove(id).then(() => this.getMembers());

      }
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  downloadPDF(pdf:string , name:string) {
    this.toastr.success('PDF is downloading !', 'Download');
    const linkSource = pdf;
    const downloadLink = document.createElement("a");
    const fileName = name+".pdf";

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }



  getMembers()
  {
     this.MS.getAllMembers().then((data)=>{
       this.liste=data;
       this.dataSource.data=this.liste;
     });
  }

  ngOnInit(): void {

    this.getMembers();

  }


}
