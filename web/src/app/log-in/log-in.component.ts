import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../Services/auth.service";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Member} from "../../Models/Member";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  //member! : Member ;

  Formuser: any;
  member: any;
  erreur = 0;

  constructor(private authService: AuthService,
              private router: Router, private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.Formuser = new FormGroup({
      cin: new FormControl(null, [Validators.required]),
      userPassword: new FormControl(null, [Validators.required]),
    })
  }

  onLoggedin() {
    console.log(this.member);
    console.log(this.Formuser.value);
    this.authService.getUserFromDB(this.Formuser.value.cin).subscribe((usr:Member) => {

      console.log(usr);
      if(usr==null)
      {
        this.toastr.error('Login ou mot de passe incorrecte !', 'Error');
      }
      else(usr.userPassword==this.Formuser.value.userPassword)
      {
        this.authService.SignIn(usr);
        this.router.navigate(['/']).then(()=>window.location.reload());
      }
    },(err) => console.log(err));
  }


}

      /*
      console.log(this.Formuser.value);
      let isValidUser: Boolean = this.authService.SignIn(this.Formuser.value);
      if (isValidUser){

         this.router.navigate(['/']).then(()=>window.location.reload());
      }

      else
        //alert('Login ou mot de passe incorrecte!');
        this.toastr.error('Login ou mot de passe incorrecte !', 'Error');*/



