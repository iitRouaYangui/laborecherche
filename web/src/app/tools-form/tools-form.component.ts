import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ToolsService} from "../../Services/tools.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Outil} from "../../Models/Outil";
import {DatePipe} from "@angular/common";
import {Member} from "../../Models/Member";
import {MemberService} from "../../Services/member.service";

@Component({
  selector: 'app-tools-form',
  templateUrl: './tools-form.component.html',
  styleUrls: ['./tools-form.component.css']
})
export class ToolsFormComponent implements OnInit {
  formtool: any;
  date: any;
  source: any;
  item1: any;
  currentId: any;
  Month: any
  dateMaxDate = new Date();
  membre:any;

  initForm(item: any): void {

    if(item?.date != null)
    {
      this.formtool = new FormGroup({
       date: new FormControl(item.date, [Validators.required]),
        source: new FormControl(item?.source, [Validators.required]),
        sourceName: new FormControl(item?.sourceName, [Validators.required]),
      })
    }
    else
    {
      this.formtool = new FormGroup({
        date: new FormControl(new Date(), [Validators.required]),
        source: new FormControl(null, [Validators.required]),
        sourceName: new FormControl(null, [Validators.required]),

      })

    }

  }

  ONSUB(): void {
    const SaveTools:Outil={...this.item1,...this.formtool.value};
    this.Toolservice.saveTools(SaveTools);
  }

  constructor(private Toolservice: ToolsService, private router: Router, private activatedRoute: ActivatedRoute ,public datepipe: DatePipe ,private MS:MemberService) {
  }

  ngOnInit(): void {
    this.currentId = this.activatedRoute.snapshot.params['id'];
    if (!!this.currentId) {
      console.log(this.currentId);
      this.Toolservice.getOutilbyId(this.currentId).then((item: Outil) => {
        this.item1 = item;
        this.initForm(this.item1)
      });
      console.log('finish');
    } else {
      this.initForm(null);
    }
    this.initForm(null)
  }

}
