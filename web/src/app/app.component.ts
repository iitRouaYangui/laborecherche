import {Component, OnInit} from '@angular/core';
import {AuthService} from "../Services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Laboratoire';

  constructor(private authService: AuthService , private router:Router){}

  ngOnInit () {
    let isloggedin: any;
    let loggedCin:any;
    let loggedUser : any;
    loggedUser= localStorage.getItem('loggedUser');
    isloggedin = localStorage.getItem('isloggedIn');
    loggedCin = localStorage.getItem('loggedCin');
    if (isloggedin!="true" || !loggedCin)
      this.router.navigate(['/login']);
    else
      this.authService.setLoggedUserFromLocalStorage(loggedCin,loggedUser);
  }

}
