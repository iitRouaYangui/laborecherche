import {Member} from "./Member";

export interface Article {
  idArticle : string ,
  type : string,
  titre:string,
  lien:string,
  date: Date,
  sourcepdf: string,
  idMembre : string | null,
  membreDTO: Member
}
