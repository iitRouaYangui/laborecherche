---17/02/2022--
create table Membre
(
    id BIGINT auto_increment,
    cin nvarchar(8) null,
    nom nvarchar(50) null,
    prenom nvarchar(50) null,
    date_naissance datetime null,
    photo varbinary(6500) null,
    cv varbinary(6500) null,
    email nvarchar(50) null,
    user_password nvarchar null,
    date_inscription datetime null,
    diplome nvarchar(50) null,
    grade nvarchar(50) null,
    etablissement nvarchar(50) null,
    type_membre nvarchar(50) null,
    code_enseignant BIGINT null,
    constraint Membre_pk
        primary key (id)
);
-- auto-generated definition
create table article
(
    id_article bigint auto_increment
        primary key,
    type       varchar(50) charset utf8  null,
    titre      varchar(50) charset utf8  null,
    lien       varchar(250) charset utf8 null,
    date       datetime                  null,
    sourcepdf  varchar(100) charset utf8 null,
    id         bigint                    null,
    constraint FKj7725unhlk1pndcej4rik7jqe
        foreign key (id) references membre (id)
);
create table evenement
(
    id_event bigint auto_increment
        primary key,
    titre    varchar(50) charset utf8 null,
    date     datetime                 null,
    lieu     varchar(50) charset utf8 null,
    id       bigint                   null,
    constraint FKqlc6sl7wd6q5pqujqhb2qsjxq
        foreign key (id) references membre (id)
);
-- auto-generated definition
create table outil
(
    id_outil bigint auto_increment
        primary key,
    date     datetime                 null,
    source   varchar(50) charset utf8 null,
    id       bigint                   null,
    constraint FKad3uxxwhssr216fk5ywirhq7d
        foreign key (id) references membre (id)
);