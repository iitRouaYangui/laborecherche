package tn.iit.pfa.laboratoire.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tn.iit.pfa.laboratoire.domain.Evenement;
import tn.iit.pfa.laboratoire.domain.Outil;
import tn.iit.pfa.laboratoire.dto.EvenementDTO;
import tn.iit.pfa.laboratoire.dto.EvenementDTOLazy;
import tn.iit.pfa.laboratoire.dto.OutilDTOLazy;

public class EvenementFactory {
  public static EvenementDTO evenementToEvenementDTO(Evenement evenement) {
    EvenementDTO evenementDTO=new EvenementDTO();
    evenementDTO.setId(evenement.getId_event());
    evenementDTO.setTitre(evenement.getTitre());
    evenementDTO.setDate(evenement.getDate());
    evenementDTO.setLieu(evenement.getLieu());
    evenementDTO.setIdMembre(evenement.getId());
    evenementDTO.setMembreDTO( MembreFactory.membreToMembreDTO(evenement.getMembre()));
    return evenementDTO;
  }

  public static Evenement evenementDTOToEvenement(EvenementDTO evenementDTO) {
    Evenement evenement=new Evenement();
    evenement.setId_event(evenementDTO.getId());
    evenement.setTitre(evenementDTO.getTitre());
    evenement.setDate(evenementDTO.getDate());
    evenement.setLieu(evenementDTO.getLieu());
    evenement.setId(evenementDTO.getIdMembre());
    evenement.setMembre(MembreFactory.membreDTOToMembre(evenementDTO.getMembreDTO()));
    return evenement;
  }

  public static EvenementDTOLazy evenementToEvenementDTOLazy(Evenement evenement) {
    EvenementDTOLazy evenementDTOLazy=new EvenementDTOLazy();
    evenementDTOLazy.setId(evenement.getId_event());
    evenementDTOLazy.setTitre(evenement.getTitre());
    evenementDTOLazy.setDate(evenement.getDate());
    evenementDTOLazy.setLieu(evenement.getLieu());
    evenementDTOLazy.setIdMembre(evenement.getId());
    return evenementDTOLazy;
  }

  public static Evenement evenementDTOToEvenementLazy(EvenementDTOLazy evenementDTOLazy) {
    Evenement evenement=new Evenement();
    evenement.setId_event(evenementDTOLazy.getId());
    evenement.setTitre(evenementDTOLazy.getTitre());
    evenement.setDate(evenementDTOLazy.getDate());
    evenement.setLieu(evenementDTOLazy.getLieu());
    evenement.setId(evenementDTOLazy.getIdMembre());
    return evenement;
  }

  public static Collection<EvenementDTO> evenementToEvenementDTOs(Collection<Evenement> evenements) {
    List<EvenementDTO> evenementsDTO=new ArrayList<>();
    evenements.forEach(x -> {
      evenementsDTO.add(evenementToEvenementDTO(x));
    } );
    return evenementsDTO;
  }
  //commentaire
}

