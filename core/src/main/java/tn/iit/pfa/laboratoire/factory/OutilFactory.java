package tn.iit.pfa.laboratoire.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tn.iit.pfa.laboratoire.domain.Membre;
import tn.iit.pfa.laboratoire.domain.Outil;
import tn.iit.pfa.laboratoire.dto.MembreDTO;
import tn.iit.pfa.laboratoire.dto.OutilDTO;
import tn.iit.pfa.laboratoire.dto.OutilDTOLazy;

public class OutilFactory {
  public static OutilDTO outilToOutilDTO(Outil outil) {
    OutilDTO outilDTO=new OutilDTO();
    outilDTO.setId(outil.getId_outil());
    outilDTO.setDate(outil.getDate());
    outilDTO.setSource(outil.getSource());
    outilDTO.setIdMembre(outil.getId());
    outilDTO.setSourceName(outil.getSourceName());
    outilDTO.setMembreDTO( MembreFactory.membreToMembreDTO(outil.getMembre()));
    return outilDTO;
  }

  public static Outil outilDTOToOutil(OutilDTO outilDTO) {
    Outil outil=new Outil();
    outil.setId_outil(outilDTO.getId());
    outil.setDate(outilDTO.getDate());
    outil.setSource(outilDTO.getSource());
    outil.setId(outilDTO.getIdMembre());
    outil.setSourceName(outilDTO.getSourceName());
    outil.setMembre(MembreFactory.membreDTOToMembre(outilDTO.getMembreDTO()));
    return outil;
  }
  public static OutilDTOLazy outilToOutilDTOLazy(Outil outil) {
    OutilDTOLazy outilDTO=new OutilDTOLazy();
    outilDTO.setId(outil.getId_outil());
    outilDTO.setDate(outil.getDate());
    outilDTO.setSource(outil.getSource());
    outilDTO.setIdMembre(outil.getId());
    outilDTO.setSourceName(outil.getSourceName());
    return outilDTO;
  }

  public static Outil outilDTOToOutilLazy(OutilDTOLazy outilDTO) {
    Outil outil=new Outil();
    outil.setId_outil(outilDTO.getId());
    outil.setDate(outilDTO.getDate());
    outil.setSource(outilDTO.getSource());
    outil.setId(outilDTO.getIdMembre());
    outil.setSourceName(outilDTO.getSourceName());
    return outil;
  }

  public static Collection<OutilDTO> outilToOutilDTOs(Collection<Outil> outils) {
    List<OutilDTO> outilsDTO=new ArrayList<>();
    outils.forEach(x -> {
      outilsDTO.add(outilToOutilDTO(x));
    } );
    return outilsDTO;
  }
  //commentaire
}

