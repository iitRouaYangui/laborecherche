package tn.iit.pfa.laboratoire.repository;

import java.lang.Long;
import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.pfa.laboratoire.domain.Membre;
@Repository
public interface MembreRepository extends JpaRepository<Membre, Long> {

    public Membre findByCin(String cin);
    Collection<Membre> findByRoleNotIn(Collection<String> role);
}

