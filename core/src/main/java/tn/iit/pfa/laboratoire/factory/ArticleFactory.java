package tn.iit.pfa.laboratoire.factory;

import tn.iit.pfa.laboratoire.domain.Article;
import tn.iit.pfa.laboratoire.domain.Outil;
import tn.iit.pfa.laboratoire.dto.ArticleDTO;
import tn.iit.pfa.laboratoire.dto.ArticleDTOLazy;
import tn.iit.pfa.laboratoire.dto.OutilDTOLazy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArticleFactory {
    public static ArticleDTO articleToArticleDTO(Article article) {
        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setIdArticle(article.getIdArticle());
        articleDTO.setType(article.getType());
        articleDTO.setTitre(article.getTitre());
        articleDTO.setLien(article.getLien());
        articleDTO.setDate(article.getDate());
        articleDTO.setSourcepdf(article.getSourcepdf());
        articleDTO.setIdMembre(article.getId());
        articleDTO.setMembreDTO( MembreFactory.membreToMembreDTO(article.getMembre()));
        return articleDTO;
    }

    public static Article articleDTOToArticle(ArticleDTO articleDTO) {
        Article article = new Article();
        article.setIdArticle(articleDTO.getIdArticle());
        article.setType(articleDTO.getType());
        article.setTitre(articleDTO.getTitre());
        article.setLien(articleDTO.getLien());
        article.setDate(articleDTO.getDate());
        article.setSourcepdf(articleDTO.getSourcepdf());
        article.setId(articleDTO.getIdMembre());
        article.setMembre(MembreFactory.membreDTOToMembre(articleDTO.getMembreDTO()));
        return article;
    }
    public static ArticleDTOLazy articleToArticleDTOLazy(Article article) {
        ArticleDTOLazy articleDTO=new ArticleDTOLazy();
        articleDTO.setIdArticle(article.getIdArticle());
        articleDTO.setType(article.getType());
        articleDTO.setTitre(article.getTitre());
        articleDTO.setLien(article.getLien());
        articleDTO.setDate(article.getDate());
        articleDTO.setSourcepdf(article.getSourcepdf());
        articleDTO.setIdMembre(article.getId());
        return articleDTO;
    }

    public static Article articleDTOToArticleLazy(ArticleDTOLazy articleDTOLazy) {
        Article article = new Article();
        article.setIdArticle(articleDTOLazy.getIdArticle());
        article.setType(articleDTOLazy.getType());
        article.setTitre(articleDTOLazy.getTitre());
        article.setLien(articleDTOLazy.getLien());
        article.setDate(articleDTOLazy.getDate());
        article.setSourcepdf(articleDTOLazy.getSourcepdf());
        article.setId(articleDTOLazy.getIdMembre());
        return article;
    }

    public static Collection<ArticleDTO> articleToArticleDTOs(Collection<Article> articles) {
        List<ArticleDTO> articlesDTO = new ArrayList<>();
        articles.forEach(x -> {
            articlesDTO.add(articleToArticleDTO(x));
        });
        return articlesDTO;
    }
    //commentaire
}

