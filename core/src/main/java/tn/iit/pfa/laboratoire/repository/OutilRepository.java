package tn.iit.pfa.laboratoire.repository;

import java.lang.Long;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.pfa.laboratoire.domain.Outil;

@Repository
public interface OutilRepository extends JpaRepository<Outil, Long> {

}

