package tn.iit.pfa.laboratoire.repository;

import java.lang.Long;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.pfa.laboratoire.domain.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
    //commentaire
}

