package tn.iit.pfa.laboratoire.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "outil")
public class Outil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_outil")
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_outil;

    @Column(name = "id")
    private Long id;

    @Column(name = "date")
    private Date date;

    @Column(name = "source")
    private String source;
    @Column(name = "name_source")
    private String sourceName;

    @JsonBackReference
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.EAGER )
    private Membre membre;

    public Long getId_outil() {
        return id_outil;
    }

    public void setId_outil(Long id_outil) {
        this.id_outil = id_outil;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

}
