package tn.iit.pfa.laboratoire.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tn.iit.pfa.laboratoire.domain.Article;
import tn.iit.pfa.laboratoire.domain.Evenement;
import tn.iit.pfa.laboratoire.domain.Membre;
import tn.iit.pfa.laboratoire.domain.Outil;
import tn.iit.pfa.laboratoire.dto.*;

public class MembreFactory {
  public static MembreDTO membreToMembreDTO(Membre membre) {
    MembreDTO membreDTO=new MembreDTO();
    membreDTO.setId(membre.getId());
    membreDTO.setCin(membre.getCin());
    membreDTO.setNom(membre.getNom());
    membreDTO.setCv(membre.getCv());
    membreDTO.setUserPassword(membre.getUserPassword());
    membreDTO.setCreateddate(membre.getCreateddate());
    membreDTO.setRole(membre.getRole());

    return membreDTO;
  }

  public static Membre membreDTOToMembre(MembreDTO membreDTO) {
    Membre membre=new Membre();
    membre.setId(membreDTO.getId());
    membre.setCin(membreDTO.getCin());
    membre.setNom(membreDTO.getNom());
    membre.setCv(membreDTO.getCv());
    membre.setUserPassword(membreDTO.getUserPassword());
    membre.setCreateddate(membreDTO.getCreateddate());
    membre.setRole(membreDTO.getRole());
    return membre;
  }
  public static MembreDTOEager membreToMembreDTO2(Membre membre) {
    MembreDTOEager membreDTO=new MembreDTOEager();
    membreDTO.setId(membre.getId());
    membreDTO.setCin(membre.getCin());
    membreDTO.setNom(membre.getNom());
    membreDTO.setCv(membre.getCv());
    membreDTO.setUserPassword(membre.getUserPassword());
    membreDTO.setCreateddate(membre.getCreateddate());
    membreDTO.setRole(membre.getRole());
    //outils
    Collection<OutilDTOLazy> outilsList =new ArrayList<>();
    if(membre.getOutils() != null){
      membre.getOutils().forEach(x-> {
        OutilDTOLazy outilDTOLazy = new OutilDTOLazy();
        outilDTOLazy = OutilFactory.outilToOutilDTOLazy(x);
        outilsList.add(outilDTOLazy);
      });
    }
    if (membreDTO.getOutilDTOS() != null) {
      membreDTO.getOutilDTOS().clear();
      membreDTO.getOutilDTOS().addAll(outilsList);
    } else {
      membreDTO.setOutilDTOS(outilsList);
    }
    membreDTO.setOutilDTOS(membreDTO.getOutilDTOS());

    // Evenement

    Collection<EvenementDTOLazy> EvenementList =new ArrayList<>();
    if(membre.getEvenements() != null){
      membre.getEvenements().forEach(x-> {
        EvenementDTOLazy evenementDTOLazy = new EvenementDTOLazy();
        evenementDTOLazy = EvenementFactory.evenementToEvenementDTOLazy(x);
        EvenementList.add(evenementDTOLazy);
      });
    }
    if (membreDTO.getEvenementDTOS() != null) {
      membreDTO.getEvenementDTOS().clear();
      membreDTO.getEvenementDTOS().addAll(EvenementList);
    } else {
      membreDTO.setEvenementDTOS(EvenementList);
    }
    membreDTO.setEvenementDTOS(membreDTO.getEvenementDTOS());

    // Article

    Collection<ArticleDTOLazy> articleList =new ArrayList<>();
    if(membre.getArticls() != null){
      membre.getArticls().forEach(x-> {
        ArticleDTOLazy articleDTOLazy = new ArticleDTOLazy();
        articleDTOLazy = ArticleFactory.articleToArticleDTOLazy(x);
        articleList.add(articleDTOLazy);
      });
    }
    if (membreDTO.getArticleDTOS() != null) {
      membreDTO.getArticleDTOS().clear();
      membreDTO.getArticleDTOS().addAll(articleList);
    } else {
      membreDTO.setArticleDTOS(articleList);
    }
    membreDTO.setArticleDTOS(membreDTO.getArticleDTOS());
    return membreDTO;
  }

  public static Membre membreDTOToMembre2(MembreDTOEager membreDTO) {
    Membre membre=new Membre();
    membre.setId(membreDTO.getId());
    membre.setCin(membreDTO.getCin());
    membre.setNom(membreDTO.getNom());
    membre.setCv(membreDTO.getCv());
    membre.setUserPassword(membreDTO.getUserPassword());
    membre.setCreateddate(membreDTO.getCreateddate());
    membre.setRole(membreDTO.getRole());
    // Outils
    Collection<Outil> outilsList =new ArrayList<>();
    if(membreDTO.getOutilDTOS() != null){
      membreDTO.getOutilDTOS().forEach(x-> {
        Outil outil = new Outil();
        outil = OutilFactory.outilDTOToOutilLazy(x);
        outilsList.add(outil);
      });
    }
    if (membre.getOutils() != null) {
      membre.getOutils().clear();
      membre.getOutils().addAll(outilsList);
    } else {
      membre.setOutils(outilsList);
    }
    membre.setOutils(membre.getOutils());

    // evenement

    Collection<Evenement> evenementList =new ArrayList<>();
    if(membreDTO.getEvenementDTOS()!= null){
      membreDTO.getEvenementDTOS().forEach(x-> {
        Evenement evenement = new Evenement();
        evenement = EvenementFactory.evenementDTOToEvenementLazy(x);
        evenementList.add(evenement);
      });
    }
    if (membre.getEvenements() != null) {
      membre.getEvenements().clear();
      membre.getEvenements().addAll(evenementList);
    } else {
      membre.setEvenements(evenementList);
    }
    membre.setEvenements(membre.getEvenements());

    //Article

    Collection<Article> articleList =new ArrayList<>();
    if(membreDTO.getArticleDTOS()!= null){
      membreDTO.getArticleDTOS().forEach(x-> {
        Article article = new Article();
        article = ArticleFactory.articleDTOToArticleLazy(x);
        articleList.add(article);
      });
    }
    if (membre.getArticls() != null) {
      membre.getArticls().clear();
      membre.getArticls().addAll(articleList);
    } else {
      membre.setArticls(articleList);
    }
    membre.setArticls(membre.getArticls());


    return membre;
  }

  public static Collection<MembreDTO> membreToMembreDTOs(Collection<Membre> membres) {
    List<MembreDTO> membresDTO=new ArrayList<>();
    membres.forEach(x -> {
      membresDTO.add(membreToMembreDTO(x));
    } );
    return membresDTO;
  }

  public static MembreDTO lazymembreToMembreDTO(Membre membre) {
    MembreDTO membreDTO=new MembreDTO();
    membreDTO.setId(membre.getId());
    membreDTO.setCin(membre.getCin());
    membreDTO.setNom(membre.getNom());
    membreDTO.setCv(membre.getCv());
    membreDTO.setUserPassword(membre.getUserPassword());
    membreDTO.setRole(membre.getRole());
    return membreDTO;
  }

  public static Collection<MembreDTO> lazymembreToMembreDTOs(Collection<Membre> membres) {
    List<MembreDTO> membresDTO=new ArrayList<>();
    membres.forEach(x -> {
      membresDTO.add(lazymembreToMembreDTO(x));
    } );
    return membresDTO;
  }
  //commentaire

}

