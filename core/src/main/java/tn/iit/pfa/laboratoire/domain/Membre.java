package tn.iit.pfa.laboratoire.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "membre")
public class Membre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "cin" )
    private String cin;

    @Column(name = "nom")
    private String nom;

    @Column(name = "cv")
    private String cv;

    @Column(name = "user_password")
    private String userPassword;

    @Column(name = "role")
    private String role;

    @Column(name = "createddate")
    private Date createddate;
    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "membre",fetch =FetchType.LAZY,orphanRemoval = true)
    private Collection<Outil> outils;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "membre",fetch =FetchType.LAZY,orphanRemoval = true)
    private Collection<Evenement> evenements;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "membre",fetch =FetchType.LAZY,orphanRemoval = true)
    private Collection<Article> articls;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCin() {
        return this.cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCv() {
        return this.cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getUserPassword() {
        return this.userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Collection<Outil> getOutils() {
        return outils;
    }

    public void setOutils(Collection<Outil> outils) {
        this.outils = outils;
    }

    public Collection<Evenement> getEvenements() {
        return evenements;
    }

    public void setEvenements(Collection<Evenement> evenements) {
        this.evenements = evenements;
    }

    public Date getCreateddate() {
        return this.createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public Collection<Article> getArticls() {
        return articls;
    }

    public void setArticls(Collection<Article> articls) {
        this.articls = articls;
    }
}
