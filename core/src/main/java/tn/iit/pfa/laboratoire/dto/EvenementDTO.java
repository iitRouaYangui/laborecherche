package tn.iit.pfa.laboratoire.dto;

import java.lang.Long;
import java.lang.String;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class EvenementDTO {
  private Long id;

  private String titre;

  private Date date;

  private Long idMembre;

  private String lieu;

  private MembreDTO membreDTO;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitre() {
    return titre;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getLieu() {
    return lieu;
  }

  public void setLieu(String lieu) {
    this.lieu = lieu;
  }

  public MembreDTO getMembreDTO() {
    return membreDTO;
  }

  public void setMembreDTO(MembreDTO membreDTO) {
    this.membreDTO = membreDTO;
  }

  public Long getIdMembre() {
    return idMembre;
  }

  public void setIdMembre(Long idMembre) {
    this.idMembre = idMembre;
  }
}

