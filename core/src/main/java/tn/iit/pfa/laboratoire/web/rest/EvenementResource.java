package tn.iit.pfa.laboratoire.web.rest;

import java.lang.Long;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import tn.iit.pfa.laboratoire.dto.EvenementDTO;
import tn.iit.pfa.laboratoire.service.EvenementService;
import tn.iit.pfa.laboratoire.util.RestPreconditions;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class EvenementResource {
  private static final String ENTITY_NAME = "evenement";

  private final EvenementService evenementService;

  private final Logger log = LoggerFactory.getLogger(EvenementService.class);

  public EvenementResource(EvenementService evenementService) {
    this.evenementService=evenementService;
  }

  @PostMapping("/addEvent")
  public ResponseEntity<EvenementDTO> createEvenement(@Valid @RequestBody EvenementDTO evenementDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
    log.debug("REST request to save Evenement : {}", evenementDTO);
    if ( evenementDTO.getId() != null) {
      bindingResult.addError( new FieldError("EvenementDTO","id","POST method does not accepte "+ENTITY_NAME+" with code"));
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    EvenementDTO result = evenementService.save(evenementDTO);
    return ResponseEntity.created( new URI("/api/evenements/"+ result.getId())).body(result);
  }


  @PutMapping("/updateEvent/{id}")
  public ResponseEntity<EvenementDTO> updateEvenement(@PathVariable Long id, @Valid @RequestBody EvenementDTO evenementDTO) throws MethodArgumentNotValidException {
    log.debug("Request to update Evenement: {}",id);
    evenementDTO.setId(id);
    EvenementDTO result =evenementService.update(evenementDTO);
    return ResponseEntity.ok().body(result);
  }

  @GetMapping("/event/{id}")
  public ResponseEntity<EvenementDTO> getEvenement(@PathVariable Long id) {
    log.debug("Request to get Evenement: {}",id);
    EvenementDTO dto = evenementService.findOne(id);
    RestPreconditions.checkFound(dto, "evenement.NotFound");
    return ResponseEntity.ok().body(dto);
  }


  @GetMapping("/events")
  public Collection<EvenementDTO> getAllEvenements() {
    log.debug("Request to get all  Evenements : {}");
    return evenementService.findAll();
  }


  @DeleteMapping("/deleteEvent/{id}")
  public ResponseEntity<Void> deleteEvenement(@PathVariable Long id) {
    log.debug("Request to delete Evenement: {}",id);
    evenementService.delete(id);
    return ResponseEntity.ok().build();
  }
}

