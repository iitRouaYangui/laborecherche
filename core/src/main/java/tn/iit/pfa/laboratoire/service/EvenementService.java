package tn.iit.pfa.laboratoire.service;

import com.google.common.base.Preconditions;
import java.lang.Long;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.pfa.laboratoire.domain.Evenement;
import tn.iit.pfa.laboratoire.dto.EvenementDTO;
import tn.iit.pfa.laboratoire.factory.EvenementFactory;
import tn.iit.pfa.laboratoire.repository.EvenementRepository;

@Service
@Transactional
public class EvenementService {
  private final Logger log = LoggerFactory.getLogger(EvenementService.class);

  private final EvenementRepository evenementRepository;

  public EvenementService(EvenementRepository evenementRepository) {
    this.evenementRepository=evenementRepository;
  }

  public EvenementDTO save(EvenementDTO evenementDTO) {
    log.debug("Request to save Evenement: {}",evenementDTO);
    Evenement evenement = EvenementFactory.evenementDTOToEvenement(evenementDTO);
    evenement = evenementRepository.save(evenement);
    EvenementDTO resultDTO = EvenementFactory.evenementToEvenementDTO(evenement);
    return resultDTO;
  }

  public EvenementDTO update(EvenementDTO evenementDTO) {
    log.debug("Request to update Evenement: {}",evenementDTO);
    Evenement inBase= evenementRepository.getById(evenementDTO.getId());
    Preconditions.checkArgument(inBase != null, "evenement.NotFound");
    Evenement evenement = EvenementFactory.evenementDTOToEvenement(evenementDTO);
    evenement = evenementRepository.save(evenement);
    EvenementDTO resultDTO = EvenementFactory.evenementToEvenementDTO(evenement);
    return resultDTO;
  }

  @Transactional(
      readOnly = true
  )
  public EvenementDTO findOne(Long id) {
    log.debug("Request to get Evenement: {}",id);
    Evenement evenement= evenementRepository.getById(id);
    EvenementDTO dto = EvenementFactory.evenementToEvenementDTO(evenement);
    return dto;
  }
  @Transactional(
      readOnly = true
  )
  public Evenement findEvenement(Long id) {
    log.debug("Request to get Evenement: {}",id);
    Evenement evenement= evenementRepository.getById(id);
    return evenement;
  }
  @Transactional(
      readOnly = true
  )
  public Collection<EvenementDTO> findAll() {
    log.debug("Request to get All Evenements");
    Collection<Evenement> result= evenementRepository.findAll();
    return EvenementFactory.evenementToEvenementDTOs(result);
  }
  public void delete(Long id) {
    log.debug("Request to delete Evenement: {}",id);
    evenementRepository.deleteById(id);
  }
}

