package tn.iit.pfa.laboratoire.web.rest;

import java.lang.Long;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import tn.iit.pfa.laboratoire.dto.OutilDTO;
import tn.iit.pfa.laboratoire.service.OutilService;
import tn.iit.pfa.laboratoire.util.RestPreconditions;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class OutilResource {
  private static final String ENTITY_NAME = "outil";

  private final OutilService outilService;

  private final Logger log = LoggerFactory.getLogger(OutilService.class);

  public OutilResource(OutilService outilService) {
    this.outilService=outilService;
  }

  @PostMapping("/addTool")
  public ResponseEntity<OutilDTO> createOutil(@Valid @RequestBody OutilDTO outilDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
    log.debug("REST request to save Outil : {}", outilDTO);
    if ( outilDTO.getId() != null) {
      bindingResult.addError( new FieldError("OutilDTO","id","POST method does not accepte "+ENTITY_NAME+" with code"));
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    OutilDTO result = outilService.save(outilDTO);
    return ResponseEntity.created( new URI("/api/outils/"+ result.getId())).body(result);
  }


  @PutMapping("/updateTool/{id}")
  public ResponseEntity<OutilDTO> updateOutil(@PathVariable Long id, @Valid @RequestBody OutilDTO outilDTO) throws MethodArgumentNotValidException {
    log.debug("Request to update Outil: {}",id);
    outilDTO.setId(id);
    OutilDTO result =outilService.update(outilDTO);
    return ResponseEntity.ok().body(result);
  }


  @GetMapping("/tool/{id}")
  public ResponseEntity<OutilDTO> getOutil(@PathVariable Long id) {
    log.debug("Request to get Outil: {}",id);
    OutilDTO dto = outilService.findOne(id);
    RestPreconditions.checkFound(dto, "outil.NotFound");
    return ResponseEntity.ok().body(dto);
  }


  @GetMapping("/tools")
  public Collection<OutilDTO> getAllOutils() {
    log.debug("Request to get all  Outils : {}");
    return outilService.findAll();
  }


  @DeleteMapping("/deleteTool/{id}")
  public ResponseEntity<Void> deleteOutil(@PathVariable Long id) {
    log.debug("Request to delete Outil: {}",id);
    outilService.delete(id);
    return ResponseEntity.ok().build();
  }
}

