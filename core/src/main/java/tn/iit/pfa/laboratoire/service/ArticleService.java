package tn.iit.pfa.laboratoire.service;

import com.google.common.base.Preconditions;
import java.lang.Long;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.pfa.laboratoire.domain.Article;
import tn.iit.pfa.laboratoire.dto.ArticleDTO;
import tn.iit.pfa.laboratoire.factory.ArticleFactory;
import tn.iit.pfa.laboratoire.repository.ArticleRepository;

@Service
@Transactional
public class ArticleService {
  private final Logger log = LoggerFactory.getLogger(ArticleService.class);

  private final ArticleRepository articleRepository;

  public ArticleService(ArticleRepository articleRepository) {
    this.articleRepository=articleRepository;
  }

  public ArticleDTO save(ArticleDTO articleDTO) {
    log.debug("Request to save Article: {}",articleDTO);
    Article article = ArticleFactory.articleDTOToArticle(articleDTO);

    article = articleRepository.save(article);
    ArticleDTO resultDTO = ArticleFactory.articleToArticleDTO(article);
    return resultDTO;
  }
  public ArticleDTO update(ArticleDTO articleDTO) {
    log.debug("Request to update Article: {}",articleDTO);
    Article inBase= articleRepository.getById(articleDTO.getIdArticle());
    Preconditions.checkArgument(inBase != null, "article.NotFound");
    Article article = ArticleFactory.articleDTOToArticle(articleDTO);
    article = articleRepository.save(article);
    ArticleDTO resultDTO = ArticleFactory.articleToArticleDTO(article);
    return resultDTO;
  }

  @Transactional(
      readOnly = true
  )
  public ArticleDTO findOne(Long id) {
    log.debug("Request to get Article: {}",id);
    Article article= articleRepository.getById(id);
    ArticleDTO dto = ArticleFactory.articleToArticleDTO(article);
    return dto;
  }
  @Transactional(
      readOnly = true
  )
  public Article findArticle(Long id) {
    log.debug("Request to get Article: {}",id);
    Article article= articleRepository.getById(id);
    return article;
  }
  @Transactional(
      readOnly = true
  )
  public Collection<ArticleDTO> findAll() {
    log.debug("Request to get All Articles");
    Collection<Article> result= articleRepository.findAll();
    return ArticleFactory.articleToArticleDTOs(result);
  }

  public void delete(Long id) {
    log.debug("Request to delete Article: {}",id);
    articleRepository.deleteById(id);
  }
}

