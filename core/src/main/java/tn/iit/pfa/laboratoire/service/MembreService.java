package tn.iit.pfa.laboratoire.service;

import com.google.common.base.Preconditions;
import java.lang.Long;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.pfa.laboratoire.domain.Membre;
import tn.iit.pfa.laboratoire.dto.MembreDTO;
import tn.iit.pfa.laboratoire.dto.MembreDTOEager;
import tn.iit.pfa.laboratoire.factory.MembreFactory;
import tn.iit.pfa.laboratoire.repository.MembreRepository;


@Service
@Transactional
public class MembreService {
  private final Logger log = LoggerFactory.getLogger(MembreService.class);

  private final MembreRepository membreRepository;

  public MembreService(MembreRepository membreRepository) {
    this.membreRepository=membreRepository;
  }


  public MembreDTO save(MembreDTO membreDTO) {
    log.debug("Request to check if cin exist: {}",membreDTO.getCin());
    Membre inBase= membreRepository.findByCin(membreDTO.getCin());
    Preconditions.checkArgument(inBase == null, "this membre already exist");
    log.debug("Request to save Membre: {}",membreDTO);
    Membre membre = MembreFactory.membreDTOToMembre(membreDTO);
    membre = membreRepository.save(membre);
    MembreDTO resultDTO = MembreFactory.membreToMembreDTO(membre);
    return resultDTO;
  }


  public MembreDTOEager update(MembreDTOEager membreDTOEager) {
    log.debug("Request to update Membre: {}",membreDTOEager);
    Membre inBase= membreRepository.getById(membreDTOEager.getId());
    Preconditions.checkArgument(inBase != null, "membre.NotFound");
    Membre membre = MembreFactory.membreDTOToMembre2(membreDTOEager);
    membre = membreRepository.save(membre);
    MembreDTOEager resultDTO = MembreFactory.membreToMembreDTO2(membre);
    return resultDTO;
  }


  @Transactional(
      readOnly = true
  )
  public MembreDTOEager findOne(Long id) {
    log.debug("Request to get Membre: {}",id);
    Membre membre= membreRepository.getById(id);
    MembreDTOEager dto = MembreFactory.membreToMembreDTO2(membre);
    return dto;
  }
  @Transactional(
          readOnly = true
  )
  public MembreDTO findOnelazy(Long id) {
    log.debug("Request to get Membre: {}",id);
    Membre membre= membreRepository.getById(id);
    MembreDTO dto = MembreFactory.membreToMembreDTO(membre);
    return dto;
  }


  @Transactional(
      readOnly = true
  )
  public Membre findMembre(Long id) {
    log.debug("Request to get Membre: {}",id);
    Membre membre= membreRepository.getById(id);
    return membre;
  }


  @Transactional(
      readOnly = true
  )
  public Collection<MembreDTO> findAll() {
    log.debug("Request to get All Membres");
    Collection<Membre> result= membreRepository.findAll();
    return MembreFactory.membreToMembreDTOs(result);
  }
  @Transactional(
      readOnly = true
  )
  public Collection<MembreDTO> findAllNotRoleIn(Collection<String> role) {
    log.debug("Request to get All Membres");
    Collection<Membre> result= membreRepository.findByRoleNotIn(role);
    return MembreFactory.membreToMembreDTOs(result);
  }

  public void delete(Long id) {
    log.debug("Request to delete Membre: {}",id);
    membreRepository.deleteById(id);
  }
}

