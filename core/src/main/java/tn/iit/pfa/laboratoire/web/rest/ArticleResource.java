package tn.iit.pfa.laboratoire.web.rest;

import java.lang.Long;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import tn.iit.pfa.laboratoire.dto.ArticleDTO;
import tn.iit.pfa.laboratoire.service.ArticleService;
import tn.iit.pfa.laboratoire.util.RestPreconditions;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class ArticleResource {
  private static final String ENTITY_NAME = "article";

  private final ArticleService articleService;

  private final Logger log = LoggerFactory.getLogger(ArticleService.class);

  public ArticleResource(ArticleService articleService) {
    this.articleService=articleService;
  }


  @PostMapping("/addArticle")
  public ResponseEntity<ArticleDTO> createArticle(@Valid @RequestBody ArticleDTO articleDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
    log.debug("REST request to save Article : {}", articleDTO);
    if ( articleDTO.getIdArticle() != null) {
      bindingResult.addError( new FieldError("ArticleDTO","id","POST method does not accepte "+ENTITY_NAME+" with code"));
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    ArticleDTO result = articleService.save(articleDTO);
    return ResponseEntity.created( new URI("/api/articles/"+ result.getIdArticle())).body(result);
  }


  @PutMapping("/updateArticle/{id}")
  public ResponseEntity<ArticleDTO> updateArticle(@PathVariable Long id, @Valid @RequestBody ArticleDTO articleDTO) throws MethodArgumentNotValidException {
    log.debug("Request to update Article: {}",id);
    articleDTO.setIdArticle(id);
    ArticleDTO result =articleService.update(articleDTO);
    return ResponseEntity.ok().body(result);
  }


  @GetMapping("/article/{id}")
  public ResponseEntity<ArticleDTO> getArticle(@PathVariable Long id) {
    log.debug("Request to get Article: {}",id);
    ArticleDTO dto = articleService.findOne(id);
    RestPreconditions.checkFound(dto, "article.NotFound");
    return ResponseEntity.ok().body(dto);
  }

  @GetMapping("/articles")
  public Collection<ArticleDTO> getAllArticles() {
    log.debug("Request to get all  Articles : {}");

    return articleService.findAll();
  }


  @DeleteMapping("/deleteArticle/{id}")
  public ResponseEntity<Void> deleteArticle(@PathVariable Long id) {
    log.debug("Request to delete Article: {}",id);
    articleService.delete(id);
    return ResponseEntity.ok().build();
  }
}

