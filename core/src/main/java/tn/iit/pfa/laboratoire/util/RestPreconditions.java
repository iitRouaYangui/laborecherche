package tn.iit.pfa.laboratoire.util;
import org.springframework.http.HttpStatus;
import tn.iit.pfa.laboratoire.web.rest.errors.MyResourceNotFoundException;



public final class RestPreconditions {

    private RestPreconditions() {
        throw new AssertionError();
    }

    // API

    /**
     * Check if some value was found, otherwise throw exception.
     *
     * @param expression
     *            has value true if found, otherwise false
     * @throws MyResourceNotFoundException
     *             if expression is false, means value not found.
     */
    /**
     * Check if some value was found, otherwise throw exception.
     *
     * @param expression has value true if found, otherwise false
     * @throws MyResourceNotFoundException if expression is false, means value not found.
     */
    public static void checkFound(final boolean expression, String message) {
        if (!expression) {
            throw new MyResourceNotFoundException("message");
        }
    }
//    public static void checkBusinessLogique(final boolean expression, String message) {
//        if (!expression) {
//
//            throw new IllegalBusinessLogiqueException(message);
//        }
//    }


    public static <T> T checkFound(final T resource) {
        if (resource == null) {
            throw new MyResourceNotFoundException();
        }

        return resource;
    }

    public static <T> T checkFound(final T resource, String message) {
        if (resource == null) {
            throw new MyResourceNotFoundException(message);
        }

        return resource;
    }

}
