package tn.iit.pfa.laboratoire.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OutilDTOLazy {
  private Long id;

  private Date date;

  private String source;
  private Long idMembre;

  private String sourceName;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getSource() {
    return source;
  }

  public Long getIdMembre() {
    return idMembre;
  }

  public void setIdMembre(Long idMembre) {
    this.idMembre = idMembre;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getSourceName() {
    return sourceName;
  }

  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

}

