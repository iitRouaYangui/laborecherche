package tn.iit.pfa.laboratoire.service;

import com.google.common.base.Preconditions;
import java.lang.Long;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.pfa.laboratoire.domain.Outil;
import tn.iit.pfa.laboratoire.dto.OutilDTO;
import tn.iit.pfa.laboratoire.factory.OutilFactory;
import tn.iit.pfa.laboratoire.repository.OutilRepository;

@Service
@Transactional
public class OutilService {
  private final Logger log = LoggerFactory.getLogger(OutilService.class);

  private final OutilRepository outilRepository;

  public OutilService(OutilRepository outilRepository) {
    this.outilRepository=outilRepository;
  }
  public OutilDTO save(OutilDTO outilDTO) {
    log.debug("Request to save Outil: {}",outilDTO);
    Outil outil = OutilFactory.outilDTOToOutil(outilDTO);
    outil = outilRepository.save(outil);
    OutilDTO resultDTO = OutilFactory.outilToOutilDTO(outil);
    return resultDTO;
  }
  public OutilDTO update(OutilDTO outilDTO) {
    log.debug("Request to update Outil: {}",outilDTO);
    Outil inBase= outilRepository.getById(outilDTO.getId());
    Preconditions.checkArgument(inBase != null, "outil.NotFound");
    Outil outil = OutilFactory.outilDTOToOutil(outilDTO);
    outil = outilRepository.save(outil);
    OutilDTO resultDTO = OutilFactory.outilToOutilDTO(outil);
    return resultDTO;
  }
  @Transactional(
      readOnly = true
  )
  public OutilDTO findOne(Long id) {
    log.debug("Request to get Outil: {}",id);
    Outil outil= outilRepository.getById(id);
    OutilDTO dto = OutilFactory.outilToOutilDTO(outil);
    return dto;
  }
  @Transactional(
      readOnly = true
  )
  public Outil findOutil(Long id) {
    log.debug("Request to get Outil: {}",id);
    Outil outil= outilRepository.getById(id);
    return outil;
  }
  @Transactional(
      readOnly = true
  )
  public Collection<OutilDTO> findAll() {
    log.debug("Request to get All Outils");
    Collection<Outil> result= outilRepository.findAll();
    return OutilFactory.outilToOutilDTOs(result);
  }
  public void delete(Long id) {
    log.debug("Request to delete Outil: {}",id);
    outilRepository.deleteById(id);
  }
}

