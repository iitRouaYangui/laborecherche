package tn.iit.pfa.laboratoire.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import tn.iit.pfa.laboratoire.domain.Article;

import java.util.Collection;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MembreDTOEager {
    private Long id;
    private String cin;
    private String nom;
    private String cv;
    private String userPassword;
    private String role;
    private Date createddate;
    private Collection<OutilDTOLazy> outilDTOS;
    private Collection<EvenementDTOLazy> evenementDTOS;
    private Collection<ArticleDTOLazy> articleDTOS;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCin() {
        return this.cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCv() {
        return this.cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getUserPassword() {
        return this.userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getCreateddate() {
        return this.createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public Collection<OutilDTOLazy> getOutilDTOS() {
        return outilDTOS;
    }

    public void setOutilDTOS(Collection<OutilDTOLazy> outilDTOS) {
        this.outilDTOS = outilDTOS;
    }

    public Collection<EvenementDTOLazy> getEvenementDTOS() {
        return evenementDTOS;
    }

    public void setEvenementDTOS(Collection<EvenementDTOLazy> evenementDTOS) {
        this.evenementDTOS = evenementDTOS;
    }

    public Collection<ArticleDTOLazy> getArticleDTOS() {
        return articleDTOS;
    }

    public void setArticleDTOS(Collection<ArticleDTOLazy> articleDTOS) {
        this.articleDTOS = articleDTOS;
    }
}
