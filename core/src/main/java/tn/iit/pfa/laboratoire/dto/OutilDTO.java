package tn.iit.pfa.laboratoire.dto;

import java.lang.Long;
import java.lang.String;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import tn.iit.pfa.laboratoire.domain.Membre;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OutilDTO {
  private Long id;

  private Date date;

  private String source;
  private Long idMembre;

  private String sourceName;

  private MembreDTO membreDTO;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getSource() {
    return source;
  }

  public Long getIdMembre() {
    return idMembre;
  }

  public void setIdMembre(Long idMembre) {
    this.idMembre = idMembre;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getSourceName() {
    return sourceName;
  }

  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  public MembreDTO getMembreDTO() {
    return membreDTO;
  }

  public void setMembreDTO(MembreDTO membreDTO) {
    this.membreDTO = membreDTO;
  }

}

