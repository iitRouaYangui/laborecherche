package tn.iit.pfa.laboratoire.web.rest;

import java.lang.Long;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import tn.iit.pfa.laboratoire.domain.Membre;
import tn.iit.pfa.laboratoire.dto.MembreDTO;
import tn.iit.pfa.laboratoire.dto.MembreDTOEager;
import tn.iit.pfa.laboratoire.repository.MembreRepository;
import tn.iit.pfa.laboratoire.service.MembreService;
import tn.iit.pfa.laboratoire.util.RestPreconditions;

/**
 * REST controller for managing Membre.
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class MembreResource {
  private static final String ENTITY_NAME = "membre";

  private final MembreService membreService;

  private final Logger log = LoggerFactory.getLogger(MembreService.class);

  public MembreResource(MembreService membreService) {
    this.membreService=membreService;
  }

  @Autowired
  MembreRepository membreRepository;
  @RequestMapping(value ="/login/{cin}",method = RequestMethod.GET)
  public Membre getUserByUserCinPassword(@PathVariable("cin") String cin) {
    return membreRepository.findByCin(cin);
  }


  @PostMapping("/addMember")
  public ResponseEntity<MembreDTO> createMembre(@Valid @RequestBody MembreDTO membreDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
    log.debug("REST request to save Membre : {}", membreDTO);
    if ( membreDTO.getId() != null) {
      bindingResult.addError( new FieldError("MembreDTO","id","POST method does not accepte "+ENTITY_NAME+" with code"));
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    MembreDTO result = membreService.save(membreDTO);
    return ResponseEntity.created( new URI("/api/membres/"+ result.getId())).body(result);
  }


  @PutMapping("/updateMember/{id}")
  public ResponseEntity<MembreDTOEager> updateMembre(@PathVariable Long id, @Valid @RequestBody MembreDTOEager membreDTO) throws MethodArgumentNotValidException {
    log.debug("Request to update Membre: {}",id);
    membreDTO.setId(id);
    MembreDTOEager result =membreService.update(membreDTO);
    return ResponseEntity.ok().body(result);
  }


  @GetMapping("/member/{id}")
  public ResponseEntity<MembreDTOEager> getMembre(@PathVariable Long id) {
    log.debug("Request to get Membre: {}",id);
    MembreDTOEager dto = membreService.findOne(id);
    RestPreconditions.checkFound(dto, "membre.NotFound");
    return ResponseEntity.ok().body(dto);
  }

  @GetMapping("/memberlazy/{id}")
  public ResponseEntity<MembreDTO> getMembrelazy(@PathVariable Long id) {
    log.debug("Request to get Membre lazy: {}",id);
    MembreDTO dto = membreService.findOnelazy(id);
    RestPreconditions.checkFound(dto, "membre.NotFound");
    return ResponseEntity.ok().body(dto);
  }


  @GetMapping("/members")
  public Collection<MembreDTO> getAllMembres() {
    log.debug("Request to get all  Membres : {}");
    return membreService.findAll();
  }


  @DeleteMapping("/membres/{id}")
  public ResponseEntity<Void> deleteMembre(@PathVariable Long id) {
    log.debug("Request to delete Membre: {}",id);
    membreService.delete(id);
    return ResponseEntity.ok().build();
  }

  @GetMapping("/membersNotRoleIn")
  public Collection<MembreDTO> findAllNotRoleIn(@RequestParam(name = "role") Collection<String> role) {

    log.debug("Request to get all  Members not role in : {}");
    return membreService.findAllNotRoleIn(role);
  }
}

