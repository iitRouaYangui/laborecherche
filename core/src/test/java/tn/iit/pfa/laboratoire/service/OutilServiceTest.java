package tn.iit.pfa.laboratoire.service;

import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.rules.ExpectedException;

import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import tn.iit.pfa.laboratoire.domain.Membre;
import tn.iit.pfa.laboratoire.domain.Outil;

import tn.iit.pfa.laboratoire.factory.OutilFactory;
import tn.iit.pfa.laboratoire.repository.OutilRepository;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;


public class OutilServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Mock
    private OutilRepository outilRepository;
    private OutilService service;
    private Outil expectedOutil;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.service = new OutilService(this.outilRepository);
        this.expectedOutil = new Outil();
        this.expectedOutil.setId_outil(2L);
        this.expectedOutil.setSourceName("ubunto");
        this.expectedOutil.setSource("https://drive.google.com/drive/u/0/folders/1QX4ZYZBV6YX1dSzpOvHmfpiNEvsfoBe7");
        this.expectedOutil.setDate(new Date(2021 - 04 - 15));
        this.expectedOutil.setId(1L);
        Membre member = new Membre();
        member.setId(1L);
        member.setCin("11185237");
        member.setRole("Teacher");
        member.setCreateddate(new Date(2022 - 04 - 15));
        member.setNom("amel");
        member.setUserPassword("1256");
        this.expectedOutil.setMembre(member);

    }

    @Test
    public void shouldSaveToolsWithSuccess() {

        when(this.outilRepository.save(any(Outil.class))).thenReturn(this.expectedOutil);

        assertThat(service.save(OutilFactory.outilToOutilDTO(expectedOutil)).getDate()).isEqualTo(expectedOutil.getDate());


    }

    @Test
    public void shouldUpdatetoolsWithSuccess() {
        given(this.outilRepository.getById(anyLong())).willReturn(
                this.expectedOutil
        );
        when(this.outilRepository.save(any(Outil.class))).thenReturn(this.expectedOutil);
        assertThat(service.update(OutilFactory.outilToOutilDTO(this.expectedOutil)).getDate()).isEqualTo(expectedOutil.getDate());
    }

}
