package tn.iit.pfa.laboratoire.service;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tn.iit.pfa.laboratoire.domain.Membre;
import tn.iit.pfa.laboratoire.factory.MembreFactory;
import tn.iit.pfa.laboratoire.repository.MembreRepository;
import java.util.Date;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.* ;


public class MembreServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Mock
    private MembreRepository membreRepository;
    private MembreService service;
    private Membre expectedmember;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.service = new MembreService(this.membreRepository);
        this.expectedmember = new Membre();
        this.expectedmember.setId(1L);
        this.expectedmember.setCin("11102890");
        this.expectedmember.setRole("Teacher");
        this.expectedmember.setCreateddate(new Date());
        this.expectedmember.setNom("ahlem");
        this.expectedmember.setUserPassword("1256");
    }

    @Test
    public void shouldSaveMemberWithSuccess() {

        when(this.membreRepository.save(any(Membre.class))).thenReturn(this.expectedmember);

        assertThat(service.save(MembreFactory.membreToMembreDTO(expectedmember)).getCin()).isEqualTo(expectedmember.getCin());

    }


    @Test
    public void shouldUpdateMemberWithSuccess() {

        given(this.membreRepository.getById(anyLong())).willReturn(
                this.expectedmember
        );
        when(this.membreRepository.save(any(Membre.class))).thenReturn(this.expectedmember);
        assertThat(this.service.update(MembreFactory.membreToMembreDTO2(this.expectedmember)).getCin()).isEqualTo(expectedmember.getCin());
    }

}
