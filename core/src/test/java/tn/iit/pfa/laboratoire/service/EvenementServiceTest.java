package tn.iit.pfa.laboratoire.service;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tn.iit.pfa.laboratoire.domain.Evenement;
import tn.iit.pfa.laboratoire.domain.Membre;
import tn.iit.pfa.laboratoire.factory.EvenementFactory;
import tn.iit.pfa.laboratoire.repository.EvenementRepository;
import java.util.Date;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

public class EvenementServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Mock
    private EvenementRepository evenementRepository;
    private EvenementService service;
    private Evenement expectedevent;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.service = new EvenementService(this.evenementRepository);
        this.expectedevent = new Evenement();
        this.expectedevent.setId_event(1L);
        this.expectedevent.setDate(new Date());
        this.expectedevent.setLieu("centre de recherche");
        this.expectedevent.setTitre("nasaespace");
        this.expectedevent.setId(1L);
        Membre member = new Membre();
        member.setId(28L);
        member.setCin("11185237");
        member.setRole("Teacher");
        member.setCreateddate(new Date(2022-04-15));
        member.setNom("amel");
        member.setUserPassword("1256");
        this.expectedevent.setMembre(member);
    }

    @Test
    public void shouldSaveEventWithSuccess()
    {

        when(this.evenementRepository.save(any(Evenement.class))).thenReturn(this.expectedevent);

        assertThat(service.save(EvenementFactory.evenementToEvenementDTO(expectedevent)).getDate()).isEqualTo(expectedevent.getDate());


    }
    @Test
    public void shouldUpdateEventWithSuccess()
    {
        given(this.evenementRepository.getById(anyLong())).willReturn(
                this.expectedevent
        );
        when(this.evenementRepository.save(any(Evenement.class))).thenReturn(this.expectedevent);
        assertThat(service.update(EvenementFactory.evenementToEvenementDTO(expectedevent)).getDate()).isEqualTo(expectedevent.getDate());

    }



}
