package tn.iit.pfa.laboratoire.service;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tn.iit.pfa.laboratoire.domain.Article;
import tn.iit.pfa.laboratoire.domain.Membre;
import tn.iit.pfa.laboratoire.factory.ArticleFactory;
import tn.iit.pfa.laboratoire.repository.ArticleRepository;
import java.util.Date;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;


public class ArticleServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Mock
    private ArticleRepository  articleRepository;
    private ArticleService service;
    private Article expectedArticle;
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.service = new ArticleService(this.articleRepository);
        this.expectedArticle = new Article();
        this.expectedArticle.setIdArticle(1L);
        this.expectedArticle.setDate(new Date());
        this.expectedArticle.setTitre("Title");
        this.expectedArticle.setDate(new Date());
        this.expectedArticle.setType("livre");
        this.expectedArticle.setId(1L);
        Membre member = new Membre();
        member.setId(28L);
        member.setCin("11185237");
        member.setRole("Teacher");
        member.setCreateddate(new Date(2022-04-15));
        member.setNom("amel");
        member.setUserPassword("1256");
       this.expectedArticle.setMembre(member);
    }

    @Test
    public void shouldSaveArticleWithSuccess()
    {
        when(this.articleRepository.save(any(Article.class))).thenReturn(this.expectedArticle);

        assertThat(service.save(ArticleFactory.articleToArticleDTO(expectedArticle)).getDate()).isEqualTo(expectedArticle.getDate());


    }

    @Test
    public void shouldUpdateArticleWithSuccess()
    {
        given(this.articleRepository.getById(anyLong())).willReturn(
                this.expectedArticle
        );
        when(this.articleRepository.save(any(Article.class))).thenReturn(this.expectedArticle);
        assertThat(service.update(ArticleFactory.articleToArticleDTO(this.expectedArticle)).getDate()).isEqualTo(expectedArticle.getDate());

    }

}
